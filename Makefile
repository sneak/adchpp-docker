export DOCKER_HOST := ssh://datavi.be

default: build

build:
	docker build -t sneak/adchpp --build-arg UBUNTU_MIRROR="http://de.archive.ubuntu.com/ubuntu" .
