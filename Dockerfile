# focal 2020.01 dev
FROM ubuntu:bionic

ENV DEBIAN_FRONTEND noninteractive

ENV DEFAULT_ADMIN_PASSWORD hunter2

ARG UBUNTU_MIRROR=http://archive.ubuntu.com/ubuntu

ARG UID_TO_ADD=10000
ARG GID_TO_ADD=10000
ARG USERNAME_TO_ADD=user

RUN echo "deb $UBUNTU_MIRROR bionic main universe restricted multiverse" > /etc/apt/sources.list.new && \
echo "deb $UBUNTU_MIRROR bionic-updates main universe restricted multiverse" >> /etc/apt/sources.list.new && \
echo "deb $UBUNTU_MIRROR bionic-security main universe restricted multiverse" >> /etc/apt/sources.list.new && \
echo "deb $UBUNTU_MIRROR bionic-backports main universe restricted multiverse" >> /etc/apt/sources.list.new && \
mv /etc/apt/sources.list.new /etc/apt/sources.list

COPY ./root /tmp/rootoverlay

RUN \
    apt update && \
    apt -y upgrade && \
    apt -y install \
        asciidoc \
        build-essential \
        ca-certificates \
        git \
        golang \
        libreadline-dev \
        locales \
        mercurial \
        libssl1.0-dev \
        python \
        rsync \
        ruby \
        ruby-dev \
        runit \
        scons \
        swig \
    && \
    go get -v -u github.com/peterbourgon/runsvinit && \
    cp /root/go/bin/runsvinit /usr/local/sbin/runsvinit && \
    rsync -avP /tmp/rootoverlay/ / && \
    rm -rf /tmp/rootoverlay && \
    rm -r /root/go && \
    groupadd -g $GID_TO_ADD $USERNAME_TO_ADD && \
    useradd -u $UID_TO_ADD -g $GID_TO_ADD -s /bin/bash $USERNAME_TO_ADD && \
    usermod -p '*' $USERNAME_TO_ADD && \
    chmod a+rx /etc/service/*/run

CMD ["/usr/local/sbin/runsvinit"]

COPY ./src /usr/local/src/adchpp

RUN \
    cd /usr/local/src/adchpp && \
    scons && \
    cd build/debug-default && \
    cp bin/adchppd /usr/local/bin && \
    cp bin/*.so /usr/local/lib && \
    ldconfig

EXPOSE 2780

VOLUME /config

