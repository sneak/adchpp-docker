# adchpp-docker

* adch++ main url: https://adchpp.sourceforge.io/
* dockerized repository: https://git.eeqj.de/sneak/adchpp-docker
* docker image: https://hub.docker.com/r/sneak/adchpp 

# Usage

```
docker run \
    -p 2780:2780 \
    -v /etc/adchpp:/config \
    --name adchpp \
    --restart unless-stopped \
    sneak/adchpp@sha256:07a8fe2e1e719a712b075590a4c94d1d3c1a49dcb3da91b0a5c971c2c17bbbf7
```

# Repo Info

`src` contains `adch++` v2.12.1 extracted from tarball from sourceforge,
modified only to make it build on modern hardware.  I tried getting it running
on 20.04 but its build file doesn't work with modern `scons`, sadly.

# Docker Image Information

* env:
    * `DEFAULT_ADMIN_PASSWORD`: set to the admin password you want for the
      `admin` user.  default: `hunter2`.  Note that passwords are stored
      unhashed in plaintext on disk (take it up with the duck, I just
      packaged it)
        * no-op if `/config/users.txt` already exists, which it will after
          first run.
* state/config volume: `/config`
* logs to stdout like a good docker

# License

GPL

# Authors

* `adch++` &copy; 2006-2016 Jacek Sieka iarnetheduck@gmail.com
* packaging/dockerizing sneak sneak@sneak.berlin
